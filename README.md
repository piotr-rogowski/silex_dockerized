# Silex Dockerized

## What's in the box?

### `Silex: v2.2.0`
### `PHP: v7.2.0`
### `MySQL: v5.7`
### `phpMyAdmin: v4.7.6`

## Prepare

1. run `composer install`
1. edit `web/index_dev.php` so it can be accessed from outside
1. build and run dockers `docker-compose up --build`

## Start

```bash
# run
docker-compose up

# stop and remove
docker-compose down
```

## Access

### App

[http://localhost/index_dev.php]() or [http://192.168.99.100/index_dev.php]()

### phpMyAdmin

[http://localhost:8080/index.php]() or [http://192.168.99.100/index.php]()

```bash
Server: db
Username: root
Password: root
```
